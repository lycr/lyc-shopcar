import Vue from 'vue';
import Vuex from 'vuex';
import { getData } from "../request/request.js";

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        name: 'lyc',
        count: 1,
        arr: [],
        allnum: 0,
        allpic: 0
    },
    getters: {
        againCount(state) {
            return state.count * 2
        }
    },
    mutations: {
        changename(state, chname) {
            state.name = chname
        },
        addcount(state) {
            state.count++
        },
        delcount(state) {
            state.count--
        },
        saveData(state, num) {
            state.arr = JSON.parse(num.data.data).categoryList
            console.log(state.arr)
        },
        addshop(state, shopnum) {
            // console.log(shopnum.flag, shopnum.ind, shopnum.pic)
            state.arr[shopnum.flag].spuList[shopnum.ind].activityType++;
            let allnum = 0;
            let allpic = 0;
            state.arr.forEach(i => {
                i.spuList.map(v => {
                    allnum += v.activityType;
                    allpic += v.activityType * v.currentPrice
                });
            });
            state.allnum = allnum
            state.allpic = allpic
        },
        delshop(state, shopnum) {
            if (state.arr[shopnum.flag].spuList[shopnum.ind].activityType > 0) {
                state.arr[shopnum.flag].spuList[shopnum.ind].activityType--;
                let allnum = 0;
                let allpic = 0;
                state.arr.forEach(i => {
                    i.spuList.map(v => {
                        allnum += v.activityType;
                        allpic += v.activityType * v.currentPrice
                    });
                });
                state.allnum = allnum
                state.allpic = allpic
            } else {
                state.arr[shopnum.flag].spuList[shopnum.ind].activityType = 0;
            }
        }
    },
    actions: {
        targetname(context) {
            context.commit('changename', 'asdfghj')
        },
        async getData(context) {
            context.commit('saveData', await getData())
        }
    }
})